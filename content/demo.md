# Demo

## Live dabei sein

Immer am ersten Mittwoch des Monats um 14 Uhr (Amsterdam/Berlin) zeigen wir online was sich neues getan hat und nehmen Feedback und Anregungen auf.
Wir freuen uns auch über mitgebrachte Themen - die gerne kurz im Community Slack ansprechen.

Nächster Termin:

* 01.02.2025, 15h (English, Zoom Link will follow here)

Einladungen werden ebenfalls über den Slack Kanal und Newsletter versendet.

## Selbst probieren

Es befinden sich folgende Pakete in Entwicklung:

 1. Der **Importer**, welcher von DOCX oder (etwas später) ODT ein online Manuscript erstellt
 1. Der **Editor**, welcher erlaubt das Manuscript und dessen Meta-Daten nachzubearbeiten
 1. Das **Template Development Kit** (TDK), welches neben Standardtemplates auch beliebige Export-Formate (etwa auch XML) erlaubt.

Unsere erste Demo unterstuetzt noch nicht alle Elemente, erkennt aber bereits Dokumentstrukturen, Gleichungen und Tabellen.

Um den Import auszuprobieren bieten wir hier ein Manuscript <a class="mdc-button" download href="/files/manuscript.docx">.docx Datei</a> und eine Bibliografie als <a class="mdc-button" download href="/files/references.bib">.bib Datei</a> zum Testen an.

<a class="mdc-button mdc-button--unelevated" href="https://os-aps.sciflow.cloud/" rel="noopen" target="_blank">Demo starten</a>

Wir freuen uns über Feedback.

*Hinweis:* Es handelt sich um eine Testumgebung. Inhalte bleiben nicht dauerhaft gespeichert.
