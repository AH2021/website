---
title: "Open Source Academic Publishing Suite"
date: 2021-02-06T18:58:57+01:00
draft: false
description: "XML-basierte Workflows zum medienneutralen Publizieren (z.B. Open Access) ohne technische Expertise mit OS-APS"
---

<p class="text-big m0">OS-APS ermöglicht XML-basierte Workflows zum medienneutralen Publizieren (z.B. Open Access) ohne technische Expertise und kostenintensive XML-Redaktions- und Content-Management-Systeme. Das Corporate Design kann über bestehende Satztemplates oder im Detail mit einem Template Development Kit gesteuert werden. Wir planen, vorhandene Open Source-Komponenten, etwa OJS, OMP und Pandoc, zu nutzen, zu erweitern und alle Ergebnisse Open Source zur Verfügung zu stellen.</p>

<div class="border-top border-bottom py2 lg-py4 mt4">
    <h2 class="mt2 mb1 h3">Projektpartner</h2>
    <div class="flex items-center center flex-wrap justify-between mxn2 col-12 lg-col-10 mx-auto">
        <div class="col-4 px2">
            <a href="//fau.de" target="_blank" rel="noopener">
                <img src="images/fau-logo.png" alt="Friedrich-Alexander-Universität Erlangen-Nürnberg logo">
            </a>
        </div>
        <div class="col-4 px2">
            <a href="//uni-halle.de" target="_blank" rel="noopener">
                <img src="images/mlu-logo.png" alt="Martin-Luther-Universität Halle-Wittenberg logo">
            </a>
        </div>
        <div class="col-4 px2">
            <a href="//sciflow.net" target="_blank" rel="noopener">
                <img src="images/sf-logo-full.svg" alt="SciFlow logo">
            </a>
        </div>
    </div>
</div>