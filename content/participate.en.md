---
title: "Join us making publishing easier"
date: 2021-02-07T18:58:57+01:00
type: single
draft: false
description: "Join us making publishing easier with OS-APS"
---

**OS-APS Advisory Boards**

Two advisory boards have been established to accompany the project constructively and productively throughout its entire duration, as well as to prioritise and steer the software development in the interests of the publishing community: The **User Advisory Board** is contacted on project questions and concerns. Furthermore, it undertakes practical testing. The **Scientific Advisory Board** fulfils an advisory function in the strategic planning and design of the project.  

The advisory boards are made up of small and medium-sized publishers, university presses, academic libraries, externally funded projects and other committed players in the field of digital publishing and open science.

Interested? If you would like to support us within the framework of the advisory boards or outside of them, this is always possible. Just get in touch with us.

Contact: [mail@os-aps.de](mailto:mail@os-aps.de)

<div class="mt4"></div>

## **Members of the Scientific Advisory Board** {#sec:noq223umxt}

<div class="col-10 my4 justify-center mx-auto flex flex-wrap logo-wrapper">
    <img src="/images/partners/Budrich_Verlag.jpg" alt="Verlag Barbara Budrich" class="m2" />
    <img src="/images/partners/Hagenhoff_FAU_Buchwissenschaft.jpg" alt="FAU Institut für Buchwissenschaf" class="m2" />
    <img src="/images/partners/Wang_IG_Digital.png" alt="IG Digital im Börsenverein" class="m2" />
    <img src="/images/partners/Sikorski_KIT.png" alt="KIT Scientific Publishing" class="m2" />
    <img src="/images/partners/Meinecke_SUB_Hamburg.png" alt="Staats- und Universitätsbibliothek Hamburg Carl von Ossietzky" class="m2" />
    <img src="/images/partners/Werner_transcript.png" alt="transcript Verlag" class="m2" />
</div>

**Verlag Barbara Budrich**

Barbara Budrich

Magdalena Lautenschlager

Miriam von Maydell

**FAU Institut für Buchwissenschaft (E-Publishing und Digitale Märkte)**

Prof. Svenja Hagenhoff

**IG Digital im Börsenverein, Peergroup Produktion**

Victor Wang

**KIT Scientific Publishing**

Joanna Sikorski

**Staats- und Universitätsbibliothek Hamburg Carl von Ossietzky**

Isabella Meinecke, Sprecherin der DINI-AG Elektronisches Publizieren

**transcript Verlag**

Dr. Karin Werner

<div class="mt2"></div>

## **Members of the User Advisory Board** {#sec:evym8om46r2}

<div class="col-10 my4 justify-center mx-auto flex flex-wrap logo-wrapper">
    <img src="/images/partners/Heuser_DUZ.png" alt="DUZ Verlags- und Medienhaus GmbH" class="m2" />
    <img src="/images/partners/Baumann_Hochschule-Merseburg.jpg" alt="Hochschule Merseburg" class="m2" />
    <img src="/images/partners/Schüßler_Schüren.png" alt="Schüren Verlag" class="m2" />
    <img src="/images/partners/Rempis_TLP.jpg" alt="Tübinger Universitätsverlage TLP" class="m2" />
    <img src="/images/partners/Rempis_TUP.jpg" alt="Tübinger Universitätsverlage TUP" class="m2" />
    <img src="/images/partners/Deppe_Unibib-Kassel.png" alt="Universität Kassel" class="m2" />
    <img src="/images/partners/Schobert_TU-Berlin-Unibib.png" alt="Universitätsbibliothek der Technischen Universität Berlin" class="m2" />
    <img src="/images/partners/Kottmann_Württembergische-Landesbib-Stuttgart.jpg" alt="Württembergische Landesbibliothek Stuttgart" class="m2" />
    <span style="flex-basis: 100%;" class="flex justify-center logo-high">
    <img src="/images/partners/Lohstraeter_Kiel.png" alt="Christian-Albrechts-Universität zu Kiel" class="m2" />
    <img src="/images/partners/Schewe_Publisso.png" alt="PUBLISSO" class="m2" />
        <img src="/images/partners/Riegler_Julius-Kühn-Institut.jpg" alt="Julius Kühn-Institut" class="m2" />
        <img src="/images/partners/Falkenstein_Feldhoff_UB-Duisburg-Essen.png" alt="Universität Duisburg-Essen" class="m2" />
    </span>
</div>

**Christian-Albrechts-Universität zu Kiel, Universitätsbibliothek, mit dem Universitätsverlag Kiel / Kiel University Publishing**

Kai Lohsträter

**DUZ Verlags- und Medienhaus GmbH**

Dr. Wolfgang Heuser

**Hochschule Merseburg, mit dem Hochschulverlag Merseburg**

Dr. Frank Baumann

**Julius Kühn-Institut - Bundesforschungsinstitut für Kulturpflanzen (Schriftenreihen mit OJS)**

Dr. Heike Riegler

**PUBLISSO / MAK-Collection**

Dr. Gisela Schewe (PubMed)

**Schüren Verlag**

Erik Schüßler

**Tübinger Universitätsverlage TUP und TLP**

Peter Rempis

**Universität Duisburg-Essen, Universitätsbibliothek, mit DuEPublico, dem laufenden BMBF-Projekt AuROA sowie dem abgeschlossenen BMBF-Projekt OGeSoMo**

Katrin Falkenstein-Feldhoff

**Universität Kassel, Universitätsbibliothek, mit kassel university press**

Arvid Deppe

**Universitätsbibliothek der Technischen Universität Berlin, mit dem Universitätsverlag der TU Berlin**

Dagmar Schobert

**Württembergische Landesbibliothek Stuttgart, Publikationsplattform zur südwestdeutschen Landesgeschichte/-kunde**

Dr. Carsten Kottmann
