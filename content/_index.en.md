---
title: "Open Source Academic Publishing Suite"
date: 2021-02-06T18:58:57+01:00
draft: false
description: "XML-based workflows for media-neutral publishing (e.g. Open Access) without technical expertise using OS-APS"
---

<p class="text-big m0">OS-APS enables XML-based workflows for media-neutral publishing (e.g. Open Access) without technical expertise and cost-intensive XML editing and content management systems. Corporate design can be controlled via existing typesetting templates or in detail with a template development kit. We plan to use and extend existing open source components, such as OJS, OMP and Pandoc, and make all results open source.</p>

<div class="border-top border-bottom py2 lg-py4 mt4">
    <h2 class="mt2 mb1 h3">Project partners</h2>
    <div class="flex items-center center flex-wrap justify-between mxn2 col-12 lg-col-10 mx-auto">
        <div class="col-4 px2">
            <a href="//fau.de" target="_blank" rel="noopener">
                <img src="images/fau-logo.png" alt="Friedrich-Alexander-Universität Erlangen-Nürnberg logo">
            </a>
        </div>
        <div class="col-4 px2">
            <a href="//uni-halle.de" target="_blank" rel="noopener">
                <img src="images/mlu-logo.png" alt="Martin-Luther-Universität Halle-Wittenberg logo">
            </a>
        </div>
        <div class="col-4 px2">
            <a href="//sciflow.net" target="_blank" rel="noopener">
                <img src="images/sf-logo-full.svg" alt="SciFlow logo">
            </a>
        </div>
    </div>
</div>